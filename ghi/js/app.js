function createCard(name, description, pictureUrl, starts, ends) {
    const startDate = new Date(starts).toLocaleDateString();
    const endDate = new Date(ends).toLocaleDateString();
    return `
        <div class="card mb-3 shadow">
            <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
            <p class="card-text fs-6 fst-italic">${description}</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">${startDate} - ${endDate}</small>
            </div>
        </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();
            let index = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details);
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const picture_url = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const html = createCard(
                        title,
                        description,
                        picture_url,
                        starts,
                        ends,
                    );
                    const column = document.querySelector(`#col-${index % 3}`);
                    column.innerHTML += html;
                    index += 1;
                }
            }
        }
    } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised
    }
});
